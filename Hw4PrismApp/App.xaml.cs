﻿using System.Diagnostics;
using Hw4PrismApp.ViewModels;
using Hw4PrismApp.Views;
using Prism;
using Prism.Ioc;
using Prism.Unity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Hw4PrismApp
{
    public partial class App : PrismApplication
    {
        //public App()
        //{
        //    InitializeComponent();

        //    MainPage = new Hw4PrismAppPage();
        //}
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer){}
       
        protected override async void OnInitialized()
        {
            Debug.WriteLine($"****{this.GetType().Name}.{nameof(OnInitialized)}");
            InitializeComponent();

            await NavigationService.NavigateAsync(nameof(Hw4PrismAppPage));

        }
        protected override void RegisterTypes(Prism.Ioc.IContainerRegistry containerRegistry)
        {
            Debug.WriteLine($"****{this.GetType().Name}.{nameof(RegisterTypes)}");
           
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<Hw4PrismAppPage, IntroToPrisimsPageViewModel>();
            containerRegistry.RegisterForNavigation<remediesForCold, remediesPageViewModel>();
        }
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
