﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Prism.Navigation;
using Prism.Commands;
using Hw4PrismApp.Views;

namespace Hw4PrismApp.ViewModels
{
    public class IntroToPrisimsPageViewModel : BindableBase, INavigationAware
    {

        INavigationService _navigationService;

        public DelegateCommand NavremediesForColdCommand { get; set; }
        private string _title;

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public IntroToPrisimsPageViewModel(INavigationService navigationService)
        {
            Debug.WriteLine($"***** {this.GetType().Name}: ctor");   

            Title = "Launch Page";
            _navigationService = navigationService;
            NavremediesForColdCommand = new DelegateCommand(OnNavToremdiesPage);
        }

        private void OnNavToremdiesPage()
        {
         Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavToremdiesPage)}");
            var navParams = new NavigationParameters();
            navParams.Add("importantData",DateTime.Now);
            _navigationService.NavigateAsync(nameof(remediesForCold),navParams);
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedFrom)}");
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedTo)}");
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}");
        }
    }
}